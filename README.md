# Project 명

## FreeRTOS_V2_CLI__F746_Template





## Project 에 대한 상세 설명

- FreeRTOS ver.2 API를 검증하고자 만드는 sample code
- STM32IDE를 기반으로 작업
- CLI code를 porting하여 추후에 CLI 가 필요한 FreeRTOS를 사용하는 project에 기본 code로 사용하기 위해 작업
- 기존 Ver 1 에서 Ver 2로 update 를 하기위한 base code임
- 작업은 STM32F746 DISCO B'D에서 검증함



## History

- 2021.03.09 : First commit
  - FreeRTOS ver 2 기반의 CLI code 를 사용하기 위한 template code 입니다
  - 추후 사용하기 위해 작업한 sample code
  - CLI code 작업만 되어있고 동작 확인 못함
  - Just commit code itself



## Development Environment

### SW Environment

- IDE 환경 : STM32CubeIDE v1.6
- Compiler 환경 : CubeIDE
- 사용된 언어 : C
- 사용된 OS : FreeRTOS v2



### HW Environment

- Chipset 정보 (정확한 part 명) : STM32F746 DISCO EVM에서 동작 검증 진행
- PCB Revsion number : MB1191 B-02
- Debugger 정보 : EVM에 내장된 ST Link V2
- Host와 연결 방식 : USB (CDC)
- Schematic location : Refer www.st.com



## Author

- Louie Yang
- Date : 2021.03.09

